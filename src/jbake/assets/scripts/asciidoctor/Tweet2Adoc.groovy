@Grab(group="org.seleniumhq.selenium", module="selenium-firefox-driver", version="3.11.0")
@Grab(group='org.asciidoctor', module='asciidoctorj', version='1.5.6')
@Grab(group='org.asciidoctor', module='asciidoctorj-pdf', version='1.5.0-alpha.16')
@GrabConfig(systemClassLoader=true)

import org.openqa.selenium.*
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxOptions

import org.asciidoctor.OptionsBuilder
import org.asciidoctor.AttributesBuilder
import org.asciidoctor.SafeMode
import org.asciidoctor.Asciidoctor.Factory

threadId = args.length ? args[0] : '994836258286456833'

System.properties.'webdriver.gecko.driver'='./geckodriver'

o = new FirefoxOptions(headless:true)

driver= new FirefoxDriver(o)
driver.manage().window().maximize()

driver.get("https://twitter.com/jagedn/status/${threadId}");

adoc = new StringBuilder()
advs = driver.findElements(By.cssSelector("div.tweet"))
advs.each{ e->
    try {
        WebElement tweet = e.findElement(By.cssSelector('p.tweet-text'))
        WebElement author = e.findElement(By.cssSelector('span.FullNameGroup'))
        WebElement avatar = e.findElement(By.cssSelector('img.avatar'))
        if( adoc.size()==0 ){
            adoc.append "= $author.text\n:data-uri:\n\nimage::${avatar.getAttribute('src')}[avatar]\n$tweet.text \n"
        }else {
            adoc.append "\n== $author.text\n$tweet.text \n"
        }
    } catch(all) {
        println all
    }
}
driver.close()

new File("${threadId}.adoc").delete()
new File("${threadId}.adoc") << adoc.toString()

asciidoctor = Factory.create()
attributes = AttributesBuilder.attributes().
        docType('article').
        tableOfContents(false).
        sectionNumbers(false).
        sourceHighlighter("coderay").
        allowUriRead(true).
        get()

options = OptionsBuilder.options(). // <2>
        backend('pdf').
        attributes(attributes).
        safe(SafeMode.UNSAFE).
        get()

asciidoctor.convertFile(new File("${threadId}.adoc"), options) // <3>