//tag::grab[]
@Grab(group='org.codehaus.gpars', module='gpars', version='1.0.0')
//end::grab[]
@GrabConfig(systemClassLoader=true)

import static groovyx.gpars.GParsPool.withPool

def lista_1 = [
    [id:1,value:5],
    [id:2,value:8],
    [id:3,value:4],
    [id:4,value:1],
    [id:5,value:2]
]
def lista_2 = [
    [id:11,value:5],
    [id:12,value:18],
    [id:13,value:14],
    [id:14,value:11],
    [id:15,value:12]
]
def lista_3 = [
    [id:1,value:15],
    [id:2,value:8],
    [id:3,value:4],
    [id:4,value:1],
    [id:5,value:2]
]
def lista_4 = [
    [id:1,value:1],
    [id:1,value:1],
    [id:2,value:2],
    [id:2,value:2],
    [id:5,value:2]

]
def lista_5 = [
    [id:1,value:1],
    [id:1,value:11],
    [id:2,value:2],
    [id:2,value:21],
    [id:5,value:2]

]
def recorrerListaHilos(lista){
  //tag::withPool[]
    withPool(10) {
        lista.eachParallel{l->
            println l
        }
    }
  //end::withPool[]
}
def unirValoresIgualesPorKey(lista_1,lista_2){
  //tag::unir[]
    def list_all = []
    lista_1.each{a->
        def row = lista_2.find{it.key == a.key}
        if (row)
            list_all << row
    }
  //end::unir[]
    return list_all
}
def mostrarDiferencias(lista_1, lista_2){
  //tag::diff[]
    def commons = lista_1.intersect(lista_2)
    def diff_part1 = lista_1.plus(lista_2)
    diff_part1.removeAll(commons)
  //end::diff[]
    return diff_part1
}
def unirValoresIguales(lista_1,lista_2){
  //tag::iguales[]
  lista_1.intersect(lista_2)
  //end::iguales[]
  return  lista_1.intersect(lista_2)
}
def ordenarPor(list,criterio){
  //tag::sort[]
    def list_sort = list.sort{it."${criterio}"}
  //end::sort[]
    return list_sort
}
def agruparPor(list,criterio){
  //tag::groupby[]
    def list_group = list.groupBy{it."${criterio}"}
  //end::groupby[]
    return list_group
}
def obtenerValoresUnicos(list){
  //tag::unique[]
    def list_unique = list.unique()
  //end::unique[]
    return list_unique
}
def eliminarValores(list,criterio){
  //tag::remove[]
    def list_rm = list.removeAll {it."${criterio}" == 1}
  //end::remove[]
    return list_rm
}
