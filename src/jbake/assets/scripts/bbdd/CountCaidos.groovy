@Grab('mysql:mysql-connector-java:5.1.6')// <1>
@GrabConfig(systemClassLoader=true)
import groovy.sql.Sql

sql=Sql.newInstance(
        "jdbc:mysql://localhost/granja", // <2>
        "user", "password", "com.mysql.jdbc.Driver" // <3>
)

row = sql.firstRow('SELECT count(*) caidos FROM hosts where lastreport < ?',[new Date()-1]) //<4>

println row?.caidos //<5>