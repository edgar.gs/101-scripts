@Grab(group='org.twitter4j', module='twitter4j-core', version='4.0.6')

import twitter4j.TwitterFactory
import twitter4j.StatusUpdate
import twitter4j.conf.ConfigurationBuilder

tf = TwitterFactory.singleton
if( new File('twitter4j.properties').exists() == false ){	//<1>
	def env = System.getenv()
	ConfigurationBuilder cb = new ConfigurationBuilder()
	cb.setDebugEnabled(true)
	  .setOAuthConsumerKey(env['CONSUMER_KEY'])
	  .setOAuthConsumerSecret(env['CONSUMER_SECRET'])
	  .setOAuthAccessToken(env['ACCESS_TOKEN'])
	  .setOAuthAccessTokenSecret(env['ACCESS_SECRET']);
	tf = new TwitterFactory(cb.build())
}
twitter = tf.instance

body = new URL("http://informo.munimadrid.es/informo/tmadrid/incid_aytomadrid.xml").newReader()
NewDataSet = new XmlSlurper().parse(body)	//<2>
NewDataSet.Incidencias.each{

	if( "$it.incid_prevista" == 'N' && "$it.incid_planificada"=='N' ){

		String tweet="""
@101GroovyScript te informa
Atención, incidencia no prevista 
$it.nom_tipo_incidencia:
$it.descripcion
"""

		try{
			twitter.updateStatus tweet	//<3>
		} catch(e){
			println "no se ha enviado"
		}
	}
}
