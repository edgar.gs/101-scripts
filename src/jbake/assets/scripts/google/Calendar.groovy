//tag::dependencies[]
@GrabResolver(name='puravida', root="https://dl.bintray.com/puravida-software/repo" )
@Grab(group='org.groovyfx',module='groovyfx',version='8.0.0',transitive=false, noExceptions=true)
@Grab(group = 'com.puravida.groogle', module = 'groogle-core', version = '1.0.0')

import com.puravida.groogle.GroogleScript
import com.google.api.services.calendar.CalendarScopes
//end::dependencies[]

import static groovyx.javafx.GroovyFX.start
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.application.Platform
import javafx.scene.SnapshotParameters
import javax.imageio.ImageIO
import java.awt.image.BufferedImage
//tag::login[]
InputStream clientSecret = new File('client_secret.json').newInputStream() //<1>

def groogle = new GroogleScript(applicationName:'101-scripts')  //<2>

groogle.login(clientSecret,[CalendarScopes.CALENDAR_READONLY]) //<3>
//end::login[]

String calendarId = 'primary'
/*
Si quieres saber todos tus calendarios simplemente ejecuta esta linea
println groogle.calendarService.calendarList().list().execute().items*.id
*/

//tag::temas[]
def colors = [
        '0':'Generico',
        '10':'Reuniones',  //<1>
        '11':'Ocio'
]
//end::temas[]

//tag::business[]
def week = ['D','L','M','X','J','V','S']
def stadistics = [:]

groogle.calendarService.events().list(calendarId).execute().items.each{ event ->   //<1>

    def events = !event.recurrentId ? [event] :  //<2>
            groogle.calendarService.events().instances(calendarId, event.id).execute().items

    events.each{ item ->
        String colorId = item.colorId ?: '0'
        if( !stadistics."${colors[colorId]}"){  //<3>
            stadistics."${colors[colorId]}" =[:]
            (java.util.Calendar.SUNDAY..java.util.Calendar.SATURDAY).each{ day ->
                stadistics."${colors[colorId]}"."${week[day-1]}"= 0
            }
        }
        int day = new Date( (event.start.date ?: event.start.dateTime).value)[java.util.Calendar.DAY_OF_WEEK]
        stadistics."${colors[colorId]}"."${week[day-1]}" +=1 //<4>
    }
}
//end::business[]

//tag::flatten[]
def flatten =[:]
stadistics.each{
    flatten."$it.key" = it.value.collect{ [it.key,it.value]}.flatten() //<1>
}
//end::flatten[]

//tag::vista[]
start {
    stage(visible:true,width:640,height:480) { //<2>
        scene{
            tilePane() {
                barChart(
                        yAxis:new NumberAxis(label:'Ocupacion', tickLabelRotation:90),
                        xAxis:new CategoryAxis(label:'Actividad'),
                        barGap:3,
                        categoryGap:20) {
                    flatten.each{   //<1>
                        series(name: it.key, data:it.value)
                    }
                }
            }
        }
    }
}
//end::vista[]
