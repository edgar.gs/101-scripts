//tag::dependencies[]
@GrabResolver(name='puravida', root="https://dl.bintray.com/puravida-software/repo" )
@Grab(group = 'com.puravida.groogle', module = 'groogle-sheet', version = '1.1.1')
@Grab('mysql:mysql-connector-java:5.1.23')
@GrabConfig(systemClassLoader=true)

import com.google.api.services.sheets.v4.SheetsScopes
import com.puravida.groogle.GroogleScript
import com.puravida.groogle.SheetScript
import groovy.sql.Sql

//end::dependencies[]

//tag::cli[]
cli = new CliBuilder(usage: '-i -s spreadSheetId g2d|d2g')
cli.with {
    h(longOpt: 'help',    args:0,'Usage Information', required: false)
    s(longOpt: 'spreadSheet', args:1, argName:'spreadSheetId', 'El id de la hoja a usar', required: true)
}
options = cli.parse(args)
if (!options) return
if (options.h || options.arguments().size()!=1) {
    cli.usage()
    return
}
if( ['g2d','d2g'].contains(options.arguments()[0]) == false ){
    cli.usage()
    return
}
google2Database = options.arguments()[0] == 'g2d'
//end::cli[]


//tag::database[]
sqlInstance = Sql.newInstance( "jdbc:mysql://localhost:3306/origen?jdbcCompliantTruncation=false", "user", "password", "com.mysql.jdbc.Driver")
List metadataTable(String table){   //<1>
    def ret = []
    sqlInstance.rows( "select * from $table limit 1".toString(),{ meta->
        (1..meta.columnCount).each{
            ret.add(meta.getColumnName(it))
        }
    })
    ret
}

void cleanTable(String table){  //<2>
    sqlInstance.execute "truncate table $table".toString()
}

void insertRowsIntoTable(List params, String table, List metadata){ //<3>
    String sql = "insert into $table (${metadata.join(',')}) values (${'?,'.multiply(metadata.size()-1)}?)"
    sqlInstance.withBatch(sql){ ps->
        params.each{ param ->
            ps.addBatch param
        }
    }
}

List moreRows(String table, int from, int size){    //<4>
    sqlInstance.rows( "select * from $table order by 1".toString(),from, size)*.values()
}
//end::database[]

//tag::login[]
clientSecret = new File('client_secret.json').newInputStream() //<1>
groogleScript = new GroogleScript('101-scripts', clientSecret,[SheetsScopes.SPREADSHEETS]) //<2>
sheetScript = new SheetScript(groogleScript: groogleScript) //<3>
//end::login[]

//tag::schemas[]
sheetScript.withSpreadSheet options.spreadSheet, { spreadSheet ->   //<1>

    sheets.each{ gSheet ->                                          //<2>
        String hojaId = gSheet.properties.title
        println "Validando schema de $hojaId"

        withSheet hojaId, { sheet ->                                //<3>

            def sheetColumns = readRows("A1", "AA1").first()        //<4>

            def tableColumns = metadataTable(gSheet.properties.title)

            assert sheetColumns.intersect(tableColumns).size() == sheetColumns.size(), """
            $gSheet.properties.title incompatible schemas:
            Sheet: $sheetColumns
            MySQL: $tableColumns
            """
        }
    }
}
//end::schemas[]

//tag::g2d[]
if( google2Database ) {
    sheetScript.withSpreadSheet options.spreadSheet, { spreadSheet ->
        sheets.each { gSheet ->
            String hojaId = gSheet.properties.title
            println "Importando hoja $hojaId"

            withSheet hojaId, { sheet ->
                def sheetColumns = readRows("A1", "AA1").first()
                cleanTable(hojaId)

                int idx = 2
                def rows = readRows("A$idx", "AA${idx + 100}")
                while (rows) {
                    insertRowsIntoTable(rows, hojaId, sheetColumns)
                    idx += rows.size()
                    rows = readRows("A$idx", "AA${idx + 100}")
                }

            }

        }
    }
}
//end::g2d[]

//tag::d2g[]
if( !google2Database ) {
    sheetScript.withSpreadSheet options.spreadSheet, { spreadSheet ->
        sheets.each{ gSheet ->
            String hojaId = gSheet.properties.title
            println "Exportando a hoja $hojaId"

            withSheet hojaId, { sheet ->
                def sheetColumns = readRows("A1", "AA1").first()
                clearRange('A','AA')

                appendRow(sheetColumns)

                int idx=0
                def rows = moreRows(hojaId, idx, 100)
                while( rows.size() ){

                    appendRows(rows)
                    idx+=rows.size()+1
                    rows = moreRows(hojaId, idx, 100)
                }
            }

        }
    }
}
//end::d2g[]