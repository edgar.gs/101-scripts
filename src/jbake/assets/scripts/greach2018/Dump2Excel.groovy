@GrabConfig(systemClassLoader=true)
@Grab(group='org.xerial', module='sqlite-jdbc', version='3.21.0')
@Grab('net.sourceforge.jexcelapi:jxl:2.6.12')

import jxl.*
import jxl.write.*
import groovy.sql.Sql

String filename = "TopSales.xls"

Sql sql = Sql.newInstance("jdbc:sqlite:greach2018.db")

WritableWorkbook workbook = Workbook.createWorkbook(new File(filename))

WritableSheet sheet = workbook.createSheet("Top", 0)

indexRow=0
sheet.addCell (new Label (0,indexRow,"Sku"))
sheet.addCell ( new Label (1,indexRow,"Product") )
sheet.addCell ( new Label (2,indexRow,"Sales") )

sql.eachRow("select * from products order by sales desc limit 3") { row ->
    sheet.addCell( new Label(0,++indexRow,"$row.id") )
    sheet.addCell( new Label(1,indexRow,row.description) )
    sheet.addCell( new Label(2,indexRow,"$row.sales") )
}
workbook.write()
workbook.close()

