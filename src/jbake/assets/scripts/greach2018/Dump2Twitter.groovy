@GrabConfig(systemClassLoader=true)
@Grab(group='org.xerial', module='sqlite-jdbc', version='3.21.0')
@Grab(group='org.groovyfx',module='groovyfx',version='8.0.0',transitive=false, noExceptions=true)
@Grab(group='org.twitter4j', module='twitter4j-core', version='4.0.6')

import groovy.sql.Sql

import static groovyx.javafx.GroovyFX.start
import javafx.application.Platform
import javafx.scene.SnapshotParameters
import javax.imageio.ImageIO
import java.awt.image.BufferedImage

import twitter4j.TwitterFactory;
import twitter4j.StatusUpdate

String message ="""
GreachConf
Greetings from the most awesome conference in 2018
#groovy-script
"""
chartTitle="Top Sales"
width=height=400
data=[:]

Sql sql = Sql.newInstance("jdbc:sqlite:greach2018.db")
sql.eachRow("select * from products order by sales desc limit 3") { row ->
    data[row.name] = row.sales as double
}

start {
    def saveNode
    stage(visible: true) {
        scene(fill: BLACK, width: width, height: height) {
            saveNode = tilePane() {
                pieChart(data: data, title: chartTitle)
            }
        }
    }
    def snapshot = saveNode.snapshot(new SnapshotParameters(), null);
    BufferedImage bufferedImage = new BufferedImage(saveNode.width as int, saveNode.height as int, BufferedImage.TYPE_INT_ARGB);
    BufferedImage image = javafx.embed.swing.SwingFXUtils.fromFXImage(snapshot, bufferedImage)

    File file = new File('screenshot.png')
    ImageIO.write(image, "png", file )


    StatusUpdate status = new StatusUpdate(message)
    status.media(file )
    TwitterFactory.singleton.updateStatus status


    Platform.exit();
    System.exit(0);
}
