@Grapes([
    @Grab(group='net.objecthunter', module='exp4j', version='0.4.8'),
    @Grab(group='org.groovyfx',module='groovyfx',version='8.0.0',transitive=false, noExceptions=true),
])

import net.objecthunter.exp4j.function.Function
import net.objecthunter.exp4j.operator.Operator
import net.objecthunter.exp4j.*

import static groovyx.javafx.GroovyFX.start
import javafx.application.Platform
import javafx.scene.SnapshotParameters
import javax.imageio.ImageIO
import java.awt.image.BufferedImage

import groovy.transform.ToString

class PlotDSL{

//tag::plotDSL[]
	List<String> _functions;
	PlotDSL(){	//<1>
		_functions=[]
	}
	PlotDSL(List<String>  functions){//<1>
		_functions = functions
	}

	PlotDSL function(String f){	// <1>
		_functions = [ f ]
		this
	}

	PlotDSL and(String f){	// <2>
		_functions << f
		this
	}

	String _variable="x"
	PlotDSL using(String variable){	// <3>
		_variable = variable
		this
	}

	double _from
	PlotDSL from(idx){
		_from=idx as double
		this
	}

	double _to
	PlotDSL to(idx){
		_to=idx as double
		this
	}

	double _steps
	PlotDSL incrementing(idx){
		_steps=idx as double
		this
	}

	Map series(){	//<4>
		calculateFunctions(_functions, _variable, _from, _to, _steps)
	}
//end::plotDSL[]

	// tag::calculateFunction[]
    private List calculateFunction(String function, String variable, double ini, double end, double step){
        Expression e = new ExpressionBuilder(function).variables(variable).build()
        def data=[]
        for(double i=ini; i<end; i+=step){
            e.setVariable(variable,i)
            double v = e.evaluate()
            data << [i, Double.isNaN(v) ? 0 : v]
        }
        data.flatten()
    }
	// end::calculateFunction[]


	// tag::calculateFunctions[]
    private Map calculateFunctions(List functions, String variable, double ini, double end, double step){
        Map ret=[:]
        functions.each{func->
            ret[func] = calculateFunction(func, variable, ini, end, step)
        }
        ret
    }
	// end::calculateFunctions[]

}

class PlotsDSL{

	List<PlotDSL> plots = []
	
	PlotsDSL parse( Closure closure ){
		Closure cl = closure.clone()
		cl.resolveStrategy = Closure.DELEGATE_ONLY
		cl.delegate=this
		cl(this)
	}

	//tag::plotsConstructor[]
	PlotsDSL plot( Closure closure){
		plot(null,closure)
	}

	PlotsDSL plot( String function, Closure closure){
		PlotDSL plot = function ? new PlotDSL( [function] ) : new PlotsDSL()
		Closure cl = closure.clone()
		cl.resolveStrategy = Closure.DELEGATE_ONLY	//<1>
		cl.delegate=plot	//<2>
		cl(plot)	//<3>
		plots << plot	//<4>
		this
	}
	//end::plotsConstructor[]

	String _screenshot
	PlotsDSL saveAs(String screenshot){
		_screenshot=screenshot
		this
	}
	
	Map series(){
		def ret=[:]
		plots.each{ plot->
			plot.series().each{ serie->
				ret[serie.key] = serie.value
			}
		}
		ret
	}
}

// tag::txtAClosure[]
txt=args.join(' ')
if( args.length == 1 && new File(args[0]).exists() ){
	txt = new File(args[0]).text
}

cl = new GroovyShell().evaluate("{ dsl-> $txt}")
plotsDSL = new PlotsDSL()
plotsDSL.parse(cl)
// end::txtAClosure[]

//tag::view[]
void saveScreenshot(saveNode, fileName){
    def snapshot = saveNode.snapshot(new SnapshotParameters(), null);
    BufferedImage bufferedImage = new BufferedImage(saveNode.width as int, saveNode.height as int, BufferedImage.TYPE_INT_ARGB);
    BufferedImage image = javafx.embed.swing.SwingFXUtils.fromFXImage(snapshot, bufferedImage)

    File file = new File(fileName)
    ImageIO.write(image, "png", file )
}

start {
    stage(id:'st',title: "GrExp4j", x: 1024, y: 768, visible: true, style: "decorated", primary: true) {
        scene(id: "sc", width: 1024, height: 768) {
	    	saveNode=lineChart(animated:true, createSymbols:false, legendVisible:true, data: plotsDSL.series())
        }
    }

    if( plotsDSL._screenshot ){
	    saveScreenshot(saveNode,plotsDSL._screenshot)
    }
}
//end::view[]