import javafx.scene.chart.Axis
@Grab(group='org.twitter4j', module='twitter4j-core', version='4.0.6')
@Grab(group='io.github.http-builder-ng', module='http-builder-ng-apache', version='1.0.3')
//@Grab(group='ch.qos.logback', module='logback-classic', version='1.2.3')

import twitter4j.*

import groovy.time.*

import static groovyx.net.http.HttpBuilder.configure
import groovyx.net.http.optional.Download
import groovyx.net.http.*

id = 996061171185840129L

width=600
height=400
sizePoint = 100

use (TimeCategory) {
    tweet = TwitterFactory.singleton.tweets().showStatus(id)
    all = TwitterFactory.singleton.getRetweets(id).sort{it.createdAt}
    first = all.last()
    last = all.first()

    lastMinute = all.collect{
        range = it.createdAt - last.createdAt
        minutes = range.minutes+(range.hours*60)+(range.days*24*60)
        minutes
    }.max()

    keyClosure = {
        range = it.createdAt - last.createdAt
        minutes = range.minutes+(range.hours*60)+(range.days*24*60)
        key = ((minutes*100)/lastMinute) as int
    }

    if( lastMinute > 60) {
        keyClosure = {
            range = it.createdAt - last.createdAt
            hours = (range.hours * 60) + (range.days * 24 * 60)
            key = ((hours * 100) / lastMinute) as int
            hours
        }
    }

    xaxis = [:]
    all.each{
        range = it.createdAt - last.createdAt
        minutes = range.minutes+(range.hours*60)+(range.days*24*60)
        key = keyClosure(it)
        xaxis["$key"] = xaxis["$key"] ? xaxis["$key"]+1 : 1
    }

    max = xaxis.values().max {it}
    weight = xaxis.collect{
        ((it.value*sizePoint)/max) as int
    }


    chartFile = configure {
        request.uri = 'https://chart.googleapis.com/chart'
        request.contentType = 'application/x-www-form-urlencoded'
        request.encoder 'application/x-www-form-urlencoded', NativeHandlers.Encoders.&form
    }.post{
        request.body = [
            'chtt'  : "@${tweet.user?.name} ${tweet.text[0..30]}",
            'cht'   : 's',
            'chxt'  : 'x,y',
            'chs'   : "${width}x${height}",
            'chxr'  : '1,' + xaxis.values().min({ it as int }) + ',' + xaxis.values().max({ it as int }),
            'chds'  : 'a',
            'chd'   : 't:'+
                    xaxis.collect{it.key}.join(',') +
                    '|'+
                    weight.join(',')+
                    '|'+
                    weight.join(',')
        ]
        println xaxis
        println request.body
        Download.toTempFile(delegate)
    }
    chartFile.renameTo(new File("${id}.png"))

    File txt = new File("${id}.txt")
    txt.delete()
    all.each{
        println it
        txt << "${it.createdAt.format('yyyy/MM/dd HH:mm:ss')};$it.user.screenName\n"
    }
}