plot {
	function "cos(sin(t))" and  "tcos(et)" and "t^4/e"

	using "t"

	from (-3) incrementing 0.01 to 3 
}

plot "sin(x*e)", {
	from (-4) to 6 incrementing 0.01 
}

saveAs "test3.png"


