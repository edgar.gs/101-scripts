@Grab('mysql:mysql-connector-java:5.1.6')
@GrabConfig(systemClassLoader=true)

import groovy.sql.Sql
import groovy.transform.BaseScript
@BaseScript Mail mail

sql=Sql.newInstance(
        "jdbc:mysql://localhost/granja",
        "user", "password", "com.mysql.jdbc.Driver"
)

row = sql.firstRow('SELECT count(*) caidos FROM hosts where lastreport < ?',[new Date()-1])


mail.sendEmail(
        "Report Status", // asunto del email
        "Existen $row.caidos servidores que no han reportado status desde hace 1 dia", //cuerpo del email
        "chron@myteam.com", //usuario_emisor
        "qa@myteam.com", //usuario_receptor
)