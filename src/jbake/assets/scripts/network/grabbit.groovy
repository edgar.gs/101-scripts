@Grapes([
    @Grab(group='com.rabbitmq', module='amqp-client', version='3.1.2'),
	@Grab(group='org.apache.commons', module='commons-vfs2', version='2.2')
])

import com.rabbitmq.client.*
import groovy.json.*

import org.apache.commons.vfs2.FileChangeEvent
import org.apache.commons.vfs2.FileListener
import org.apache.commons.vfs2.FileObject
import org.apache.commons.vfs2.FileSystemManager
import org.apache.commons.vfs2.VFS
import org.apache.commons.vfs2.impl.DefaultFileMonitor

//tag::conexion[]
exchangeName="groovy-script"
queueName="grabbit"
routingKey='#'

factory = new ConnectionFactory()
	factory.username='guest'
	factory.password='guest'
	factory.virtualHost='/'
	factory.host= args[0] ?: 'localhost'
	factory.port=5672
conn = factory.newConnection()

channel = conn.createChannel()
channel.exchangeDeclare(exchangeName, "direct", true)
channel.queueDeclare(queueName, true, false, false, null)
channel.queueBind(queueName, exchangeName, routingKey)
//end::conexion[]

if( args[1] == 'producer' ) {

	//tag::producer[]
	monitor = args[2]

	if (new File(monitor).exists() == false)
		new File(monitor).newPrintWriter().println "${new Date()}"

	FileSystemManager manager = VFS.getManager();
	FileObject fobj = manager.resolveFile(new File(monitor).absolutePath)
	DefaultFileMonitor fm = new DefaultFileMonitor(this as FileListener)
	fm.delay = 500
	fm.addFile(fobj)
	fm.start()
	//end::producer[]

}

if( args[1] == 'consumer' ) {
	//tag::consumer[]
	boolean autoAck = true;
	channel.basicConsume(queueName, autoAck, new DefaultConsumer(channel) {
         public void handleDelivery(String consumerTag,
                                    Envelope envelope,
                                    AMQP.BasicProperties properties,
                                    byte[] body)
             throws IOException{
		println "Soy el consumer recibiendo ${new String(body)}"
         }
	});
	//end::consumer[]
}

//tag::filechanged[]
void fileChanged(FileChangeEvent evt) throws Exception {

	RandomAccessFile randomAccessFile = new RandomAccessFile(evt.file.localFile, "r");	//<1>
	long fileLength = evt.file.localFile.length() - 1;
	randomAccessFile.seek(fileLength);
	StringBuilder stringBuilder = new StringBuilder()

	for(long pointer = fileLength; pointer >= 0; pointer--){
		randomAccessFile.seek(pointer)
		char c = (char)randomAccessFile.read()
		if(c == '\n' && fileLength == pointer){
			continue
		}
		if(c == '\n' && fileLength != pointer){
			break
		}
		stringBuilder.append(c)
	}
	println "Soy el producer enviando ${stringBuilder.toString().reverse()}"
	channel.basicPublish(exchangeName, routingKey, null, stringBuilder.toString().reverse().bytes) //<2>
}
//end::filechanged[]

void fileCreated(FileChangeEvent arg0) throws Exception {

}

void fileDeleted(FileChangeEvent arg0) throws Exception {

}
