//tag::grab[]
@Grab('org.apache.poi:poi:3.8')
@Grab('org.apache.poi:poi-ooxml:3.8')
@Grab('mysql:mysql-connector-java:5.1.6')
//end::grab[]

import org.apache.poi.ss.usermodel.*
import org.apache.poi.hssf.usermodel.*
import org.apache.poi.xssf.usermodel.*
import org.apache.poi.ss.util.*
import org.apache.poi.ss.usermodel.*
import java.io.*
import groovy.sql.Sql
//tag::mysql[]
def sql = Sql.newInstance( "jdbc:mysql://localhost:3306/origen?jdbcCompliantTruncation=false",
        "user",
        "password",
        "com.mysql.jdbc.Driver")
//end::mysql[]
//tag::variables[]
def table = "myTable"
def path_file = "${System.properties['user.home']}/myExcel.xlsx"
def list = parse(path_file)
//end::variables[]
insertValues(table,list,sql)
//tag::insert[]
def insertValues(table,list,sql){
  int index = 0
  sql.rows(""" desc $table """.toString()).each{c-> // <1>
        keys = index == 0?(keys + ":$c.Field"):(keys + ",:$c.Field")
        index++

  }
  sql.withBatch(20,   """ insert into $table values ( $keys ) """.toString()) { ps -> // <2>
      list.each { t->
        ps.addBatch(t)
      }
  }
}
//end::insert[]
//tag::parse[]
def parse(path) {
    InputStream inp = new FileInputStream(path)
    Workbook wb = WorkbookFactory.create(inp);
    Sheet sheet = wb.getSheetAt(0);// <1>

    Iterator<Row> rowIt = sheet.rowIterator() // <2>
    Row row = rowIt.next()
    def headers = getRowData(row) // <3>

    def rows = []
    while(rowIt.hasNext()) {
      row = rowIt.next()
      rows << getRowData(row)
    }
    [headers, rows]
}
//end::parse[]
//tag::row[]
def getRowData(Row row) {
    def data = []
    for (Cell cell : row) {
      getValue(row, cell, data)
    }
    data
}
//end::row[]
//tag::value[]
def getValue(Row row, Cell cell, List data) {
    def rowIndex = row.getRowNum()
    def colIndex = cell.getColumnIndex()
    def value = ""
    switch (cell.getCellType()) {
      //tag::string_f[]
      case Cell.CELL_TYPE_STRING:
        value = cell.getRichStringCellValue().getString();
        break;
    //end::string_f[]
      case Cell.CELL_TYPE_NUMERIC:
        if (DateUtil.isCellDateFormatted(cell)) {
            value = cell.getDateCellValue();
        } else {
            value = cell.getNumericCellValue();
        }
        break;
      case Cell.CELL_TYPE_BOOLEAN:
        value = cell.getBooleanCellValue();
        break;
      case Cell.CELL_TYPE_FORMULA:
        value = cell.getCellFormula();
        break;
      default:
        value = ""
    }
    data[colIndex] = value
    data
}
//end::value[]
