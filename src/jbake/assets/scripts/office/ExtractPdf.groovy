//tag::dependencies[]
@Grapes(
        @Grab(group='org.apache.pdfbox', module='pdfbox', version='2.0.8')
)
import org.apache.pdfbox.pdmodel.PDDocument
import org.apache.pdfbox.text.*
import java.awt.Rectangle
//end::dependencies[]

//tag::sourceCode[]
margenright = 10    //<1>

PDDocument document = PDDocument.load(new URL(args[0]).bytes)

def pages = [:]
document.documentCatalog.pages.eachWithIndex { page , pageIndex->

    def paragraphs = [:]

    PDFTextStripperByArea stripper = new PDFTextStripperByArea(sortByPosition:true)
    (0..42).each {  //<2>
        stripper.addRegion("$it", new Rectangle( margenright, it * 20, 1500, 20));  //<3>
    }
    stripper.extractRegions(page)

    stripper.regions.eachWithIndex{ r, index->
        def str = stripper.getTextForRegion(r)
        if (str.trim().length() > 0)
            paragraphs["$index"] = str.trim()       //<4>
    }
    pages["$pageIndex"] = paragraphs    //<5>
}
println pages
//end::sourceCode[]
