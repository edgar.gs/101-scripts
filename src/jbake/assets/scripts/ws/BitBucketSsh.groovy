//tag::dependencies[]
@Grab(group='io.github.http-builder-ng', module='http-builder-ng-apache', version='1.0.3')
@Grab(group='ch.qos.logback', module='logback-classic', version='1.2.3')
import groovyx.net.http.*
import static groovyx.net.http.HttpBuilder.configure
import static groovyx.net.http.ContentTypes.JSON
import static groovy.json.JsonOutput.prettyPrint
import static groovy.json.JsonOutput.toJson
//end::dependencies[]

//tag::login[]
organization = args[0] // organization
username = args[1]  //secretId
password = args[2]  //token

creds = "$username:$password".bytes.encodeBase64()

def http = configure {
    request.uri = 'https://bitbucket.org/'
    request.headers['Authorization'] = "Basic $creds"
    request.contentType = JSON[0]
}

def token = http.post{
    request.uri.path='/site/oauth2/access_token'
    request.body=[grant_type:'client_credentials']  //<1>
    request.contentType = 'application/x-www-form-urlencoded'
    request.encoder 'application/x-www-form-urlencoded', NativeHandlers.Encoders.&form
}.access_token
//end::login[]

//tag::bitbucket[]
def bitbucket = configure {
    request.uri = 'https://api.bitbucket.org/'
    request.headers['Authorization'] = "Bearer $token"
    request.accept=['application/json']
    request.contentType = JSON[0]
}
//end::bitbucket[]

//tag::repos[]
def page=1
def repos=[]
while( true ) {
    def list = bitbucket.get{
        request.uri.path="/2.0/repositories/$organization"
        request.uri.query=[page:page]
    }
    repos.addAll list.values
    if( repos.size() >= list.size )
        break
    page++
}
//end::repos[]

//tag::v10[]
def keys=[:]    //<1>
repos.findAll{it.slug}.each{  repo->
    def repokeys = bitbucket.get{
        request.uri.path="/1.0/repositories/$organization/${repo.slug}/deploy-keys"
    }
    keys[repo.slug]=repokeys
}
//end::v10[]

//tag::report[]
println "Total repos founded : ${repos.size()}"
println "Total keys founded : ${keys.size()}"
println prettyPrint(toJson(keys))

def remove = keys.findAll{ it.value.find{ it.key.indexOf('XXXXXXXXXXXXXXXXXXX')!=-1 } } //<1>
println "Remove: ${remove*.key}"
//end::report[]