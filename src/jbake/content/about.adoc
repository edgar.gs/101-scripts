= Acerca de
Jorge Aguilera
2017-08-01
ifndef::backend-pdf[]
:jbake-type: page
:jbake-status: published
:jbake-tags: blog, asciidoc
:idprefix:
endif::[]


101 (o menos) Scripts de Groovy pretende ser una serie de entradas donde desarrollar paso a paso diferentes scripts
hechos en este lenguaje con un carácter eminentemente práctico y que puedan servir como base para aplicarlos en tu
día a día.

La ídea final es conseguir generar material suficiente como para publicarlo en formato libro electrónico y ponerlo a la
venta DESTINANDO LOS POSIBLES BENEFICIOS A PROGRAMAS DE FORMACIÓN JUVENIL

Cualquiera puede participar en este proyecto. Simplemente haznolo saber y hablamos. Actualmente los autores son:

- Jorge Aguilera, jorge.aguilera@puravida-software.com

- Miguel Rueda, miguel.rueda.garcia@gmail.com

- Roberto Battista, robybattista@gmail.com


