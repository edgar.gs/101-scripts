= Product catalog with Asciidoctor
Miguel Rueda <miguel.rueda.garcia@gmail.com>
2018-01-25
ifndef::backend-pdf[]
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:jbake-category: asciidoctor
:jbake-script: /scripts/asciidoctor/Catalogo.groovy
:idprefix:
:imagesdir: ../images
:jbake-spanish: catalogo
:jbake-lang: gb
endif::[]

In this case we will do a product catalog from products of our store. It will be a Pdf with some articles and we'll show
the description,  prize and a picture of the product.

:icons: font
NOTE: We'll use *http://asciidoctor.org/[Asciidoctor]*. In this link you can find a basic tutorial about it (spanish)
http://jorge-aguilera.gitlab.io/tutoasciidoc/[tutorial basico].

NOTE: In this example we'll use pictures from our local filesystem but also we can use links to a remote repository via
http. In our case the id of every item will be the name of the picture.

NOTE: Our schema will be very simple: only one table with the id, the description and the price of the article.
{set:icons!:}

== Considerations

Our script requires a database with the information of articles and for every one it'll generate a page in the Pdf.
We'll use a template file common to all of them, although using a different one based on category, price, etc of each item is a trivial change.

.product.tpl
....
.Ref: ${sku}
----
Description: ${description}

SKU ${sku}, price ${price}
----

image::${sku}.png[${description}]
....


.catalog.tpl
....
= Catalog
Miguel Rueda <miguel.rueda.garcia@gmail.com>
:idprefix:
:icons: font
:imagesdir: ./images

This is our catalog of products. We hope you'll find the references you are looking for. Don't hesitate to contact
with us at 555-555-555

....


== Environment

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=prepararEntorno]
----

First of all we need to load the templates (product.tpl and catalog.tpl). Also we'll need the query to read the products.

After them, the script will clean the temporary files who can be present from previous executions (files .adoc in our case)

As we want to replace the template with the values of every item we'll use a _SimpleTemplateEngine_


== Products

The script will iterate the select and generate one file per every item using the template _product.tpl_

=== Loading

The `generateProducts()` function connect with the database, execute the _query_ (initialized at the beginning) and
for every item will call `createProductAdoc` function:

.generateProducts
[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=generateProducts]
----

=== Dump product file

Using the _engine_ we'll mix the values of the item with the template, generating one file per article:

.createProductAdoc
[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=create_adoc]
----

== Catalog file.

To build the final *catalogo.adoc* file we will:

- use the _catalog.tpl_ template file to customize the font page and asciidoc headers
- include previously generated files

.generateCatalog
[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=create_catalog]
----

== PDF

At the end we need to call _Asciidoctor_ to generate the pdf using _catalog.adoc_ :

._cretatePDF_
[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=create_pdf]
----
<1> We can select different attributes as the table of content, sections, etc
<2> In this case we want a Pdf
<3> call Asciidoctor to generate the _catalog.pdf_


=== Customization

If you want you can customize the pdf with a "theme". You only need a `yml` file to customize the title,
font, backgrounds etc. For example:

.mytheme.yml
----
title_page: // <1>
  align: left
base:
  font_family: Times-Roman // <2>
  font_size: 12  // <3>
----

<1> We want the title align left
<2> We want `Times-Roman` as the font
<3> We can choose the size of the font

We need to indicate to _Asciidoctor_ we want to apply this theme using the attribute _pdf-style_:

[source,groovy]
----
    asciidoctor = Factory.create();
    attributes = AttributesBuilder.attributes().attribute("pdf-style", "tema.yml").
        docType('book').
        tableOfContents(true).
        sectionNumbers(true).
        get()
----