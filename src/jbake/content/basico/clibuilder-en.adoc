= CliBuilder
Miguel Rueda <miguel.rueda.garcia@gmail.com>
2017-08-30
ifndef::backend-pdf[]
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:jbake-category: basico
:jbake-script: /scripts/basico/CliBuilder.groovy
:jbake-lang: gb
:idprefix:
:imagesdir: ../images
:jbake-spanish: clibuilder
endif::[]

You can have the situation to having a script that performs certain functions that we want to parameterize.

The easiest option is to use the variable * args * implicit in the script that is an array of String that contains
the parameters that are passed after the filename.

However, you also have `CliBuilder`, a Groovy utility that allows you to make the arguments that you can
pass to a script be more explicit.

A very basic script using this tool could be the following:

[source,ruby]
----
include::{sourcedir}{jbake-script}[]
----
<1> We define the parameters that we will need.
<2> It will show the legend of our command.
<3> This part is executed when sending as parameter `-a`.

If for example we call our script passing the parameter `-h` we will get the legend of our command:

----
groovy clibuilder_ebook.groovy  -h
usage: clibuilder_ebook.groovy -[habcd]
 -a,--Option a   When selecting "a" write seleccionada -> a
 -b,--Option b   When selecting "b" write seleccionada -> b
 -c,--Option c   When selecting "c" write seleccionada -> c
 -d,--Option d   When selecting "d" write seleccionada -> d
 -h,--help       Usage Information
----
