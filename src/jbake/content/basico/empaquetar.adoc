= Empaquetado Scripts
Jorge Aguilera <jorge.aguilera@puravida-software.com>
2017-11-14
ifndef::backend-pdf[]
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:jbake-category: basico
:jbake-script: /scripts/basico/HolaMundo.groovy
:idprefix:
:imagesdir: ../images
:jbake-english: empaquetar-en
endif::[]

En este artículo vamos a tratar cómo podemos agrupar nuestros scripts en un jar de tal forma
que sean accesibles al resto del equipo (o del mundo) de una forma cómoda y sencilla.

Como puedes ver, en el resto de artículos tratamos scripts sueltos que cumplen una función
(buscar ficheros, transformar datos, invocar servicios web, etc) pero puede darse la situación
que todos, o parte de ellos, sean de utilidad para ciertos usuarios en diferentes situaciones
y queremos que puedan utilizarlos bien sea ejecutándolos directamente o bien
como base para otros scripts.

Así por ejemplo podríamos tener las siguientes situaciones:

- Scripts de utilidad directa
- Scripts base destinados a ser personalizados (extendidos) por otros
- Scripts con lógica de negocio que nos interesa ocultar.
- Scripts que dependen de otros para su ejecución correcta
- etc

== Directorio compartido

La primera solución que se nos presenta para poder reutilizar nuestros scripts es ubicarlos
en una carpeta compartida, bien sea en una carpeta de red por ejemplo SAMBA o bien en una
carpeta local de una máquina a la que podemos acceder.

La ventaja de esta solución obviamente es la sencillez con el añadido que si dicha carpeta
se encuentra versionada (SVN, Git, etc) es muy fácil tenerla actualizada

Obviamente una desventaja de estos scripts es que estarán restringuidos a ejecutarse
en el entorno de esta máquina.
Es decir si por ejemplo tenemos un script que escanea un directorio buscando un fichero
determinado para trabajar con él sólo tendría sentido buscarlo en esta máquina y no en la
nuestra por ejemplo.

== Alojado en SVN/Git

Otra opción es disponer de un SCM (source control manager) tipo subversion SVN, git o similar
donde los usuarios puedan clonarse el proyecto en sus máquinas y ejecutarlos desde ellas.

Así simplemente mediante comandos de de actualización como por ejemplo `git pull` refrescaríamos
el directorio local con la última versión y podríamos ejecutar el comando.

Sin embargo, a parte de que necesitamos instalar el control de versiones en la máquina donde
queramos ejecutar los scripts, tendríamos que revisar que hacemos el _pull_ de forma habitual
etc

== Servidor HTTP

Gracias a la capacidad de Groovy de poder ejecutar scripts en URL remotas podemos hacer que
los scripts se alojen en un servidor web (Apache, Nginx, o similar) y que los usuarios los
invoquen vía http

[source,console]
----
groovy http://groovy-lang.gitlab.io/101-scripts/scripts/office/ExtractPdf.groovy https://www.boe.es/boe/dias/2017/09/21/pdfs/BOE-B-2017-54046.pdf
----

Como podemos ver en este ejemplo sólo necesitamos tener Groovy instalado y ejecutar
scripts que residen en un servidor web a la vez que le pasamos argumentos

== Jar

Utilizando la capacidad de Groovy de poder ejecutar scripts contenidos en un zip/jar
podemos agrupar nuestros ficheros con un simple comando:

[source,console]
----
java cvf mis_scripts.jar *.groovy //<1>
groovy jar:file:mis_scripts.jar'!'/FindFile.groovy  //<2>
----
<1> Empaquetamos nuestros scripts en un jar (lo que viene siendo un zip)
<2> Mediante esa URI conseguimos ubicar nuestro script dentro del jar y así poder ejecutarlo

== Publicando Jar

Si disponemos de un servidor Maven (o una cuenta en MavenCentral, Bintray, etc) podremos así mismo
publicar este jar para poder usarlo mediante @Grape en otros scripts.

En este apartado vamos a explicar
cómo preparar un proyecto Gradle que nos permita generar el Jar y publicarlo en Bintray para que el resto de nuestros
scripts puedan usarlos (por lo que deberás tener una cuenta creada en Bintray)

=== Estructura directorios

Establece una estructura de directorios como el de la imagen. *Nuestros scripts se ubicarán en el directorio resources*


....
+-------------+
| mis_scripts |
+-------------+
  |
  | +-----+
  +>| src |
    +-----+
       |    +------+
       +--->| main |
            +------+
               |    +-----------+
               +--->| resources |
                    +-----------+
....

=== Gradle

En el directorio raiz _mis_scripts_ copia el siguiente fichero:

.build.gradle
[source,groovy]
----
buildscript {
    repositories {
        jcenter()
    }
    dependencies {
        classpath 'com.jfrog.bintray.gradle:gradle-bintray-plugin:1.7.3'
    }
}

apply plugin: 'groovy'
apply plugin: 'java-library-distribution'
apply plugin: 'maven'
apply plugin: 'maven-publish'
apply plugin: 'com.jfrog.bintray'

repositories {
    mavenCentral()
}

dependencies {
    compile 'org.codehaus.groovy:groovy-all:2.4.12'
    testCompile 'junit:junit:4.12'
}

javadoc {
    title = "$project.description $project.version API"
}

task sourceJar(type: Jar, dependsOn: classes) {
    classifier = 'sources'
    from sourceSets.main.allSource
}

task javadocJar(type: Jar) {
    classifier = "javadoc"
    from javadoc
}

distZip.shouldRunAfter(build)
publishing {
    publications {
        maven(MavenPublication) {
            artifactId 'mis-scripts'  //<1>
            from components.java
            artifact sourceJar {
                classifier "sources"
            }
        }
    }
}
bintray {
    user = System.getenv("BINTRAY_USER") ?: project.hasProperty("bintrayUser") ? project.bintrayUser : ''  //<2>
    key = System.getenv("BINTRAY_KEY") ?: project.hasProperty("bintrayKey") ? project.bintrayKey : ''
    publications = ['maven']
    publish = true
    pkg {
        repo = "mi-repositorio"   // <3>
        userOrg = project.hasProperty('userOrg') ? project.userOrg : 'mi-organization'  //<4>
        name = "mis-scripts"
        desc = "Groovy Scripts"
        websiteUrl = "https://tuwebsite.com"
        licenses = ['Apache-2.0']
        publicDownloadNumbers = true
        version {
            name = project.version
        }
    }
}
----
<1> Indica el nombre del artefacto que quieres publicar en Bintray
<2> Configuración de user/token por variable de entorno, gradle.properties o valor por defecto
<3> El nombre del repositorio donde quieres publicar en Bintray (tienes que haberlo creado antes en Bintray)
<4> El nombre de tu organización (tienes que haberlo creado antes en Bintray)

=== Publicando

Para empaquetar y publicar tu artefacto simplemente ejecuta:

[source]
----
gradle bintrayUpload
----

Si todo va bien, se habrá creado tu jar con los scripts embebidos en él y se habrá subido a Bintray

=== "Grapeando"

A partir de aqui nuestro artefacto es accesible mediante cualquier gestor de dependencias como puede ser Maven o
Gradle y por supuesto *Grape* por lo que podremos crear nuevos scripts que dependan de nuestro artefacto simplemente
poniendo las "coordenadas" (repositorio, organización, artefacto y versión) oportunas en el script


