= Instalación y primeros pasos
Miguel Rueda <miguel.rueda.garcia@gmail.com>
2017-08-02
ifndef::backend-pdf[]
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:jbake-category: basico
:jbake-script: /scripts/basico/HolaMundo.groovy
:idprefix:
:imagesdir: ../images
:jbake-english: instalacion-en
endif::[]

Groovy es un lenguaje de programación dinámico para la máquina virtual de java (Java Virtual Machine). Esto quiere decir
que el código que genera puede ser ejecutado por el Runtime de Java y por ello se requiere tener instalado Java 6+
(recomendado Java 8 y muy pronto Java 9)

== Usando los binarios

Simplemente hay que descargarse los binarios desde la página oficial, descomprimirlo en un directorio a nuestro antojo
y ajustar las variables de entorno *GROOVY_HOME* y *PATH*

== Windows

Existe un instalador de Windows basado en NSIS que nos permitirá instalar y desinstalar Groovy en nuestra máquina

== Maven (y similares)

Es posible también embeber Groovy en nuestros proyectos utilizando los artefactos:

[source,xml]
----
<groupId>org.codehaus.groovy</groupId>
<artifactId>groovy</artifactId>
<version>2.4.12</version>
----


== SdkMan

Para incorporar la herramienta groovy recomandamos instalar SdkMan (http://sdkman.io/). Este software es muy práctico
tanto para instalar como para manejar las diferentes versiones de las herramientas instaladas.

Con el sdkman en nuestro sistema realizar desde la consola.

    sdk install groovy

== Comprobación

Una vez finalizado el comando de instalación pasaremos a comprobar que tenemos el groovy preparado.

    groovy -version

=== GroovyConsole

Ya podemos lanzar nuestra primera consola de groovy

    groovyConsole

En pocos segundos se abrirá nuestra consola de groovy donde podemos empezar a trabajar con los primeros scripts

image::groovy_console.jpg[]

Para realizar una prueba sencilla podemos ejecutar nuestro primer "Hola Mundo":

.HolaMundo.groovy
[source, groovy]
----
include::{sourcedir}{jbake-script}[]
----

Mediante el menú de GroovyConsole podemos ejecutar el script y visualizar el resultado en el panel inferior.

=== Consola

La consola de groovy es muy útil cuando estamos desarrollando nuestro script porque nos permite ejecutarlo,
corregirlo y volver a ejecutarlo en un mismo sitio. Sin embargo también podemos usar nuestro editor
de texto preferido (que no es el Word) como puede ser Notepad, Notepad++, gedit, vi, emacs, etc y ejecutarlo
desde consola (o embebido en un bash por ejemplo)

[source,bash]
----
groovy HolaMundo.groovy
----


== Groovy Web Console

Por último, también puedes probar a utilizar la consola on-line https://groovyconsole.appspot.com/
la cual te permitirá ejecutar algunos scripts sencillos (además de compartirlos de forma pública)

