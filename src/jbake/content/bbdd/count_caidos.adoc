= Contar registros de base de datos
Jorge Aguilera, jorge.aguilera@puravida-software.com
2017-08-26
ifndef::backend-pdf[]
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:jbake-category: bbdd
:jbake-script: /scripts/bbdd/CountCaidos.groovy
:idprefix:
:imagesdir: ../images
:jbake-english: count_caidos-en
endif::[]


Supongamos que tenemos una tabla en una base de datos MySQL donde cada servidor de nuestra red reporta su estado.
Digamos que cada servidor realizar un insert/update en la tabla actualizando un campo *lastupdate* con la fecha
del sistema para poder saber así cuando fue la última actualización de ese servidor.

Como sysadmin te interesa saber en un momento determinado cúantos servidores se encuentran caídos desde un momento
determinado (desde hace 1 minuto, 1 hora, 1 día ... it's up to you) para poder determinar si tienes problemas.

Con este script simplemente te conectas a la base de datos, realizas una consulta COUNT y muestras el resultado por
pantalla. Esta salida podrás concatenarla con algún otro programa/script que te permita reaccionar cuando el número
supera un umbral determinado como enviar una alarma, etc.

[source,groovy]
----
include::{sourcedir}{jbake-script}[]
----
<1> Cargamos la dependencia al driver MySQL
<2> Indicamos el host y la base de datos alojada
<3> Indicamos así mismo datos necesarios para la conexion como usuario y password
<4> Buscamos el primer registro que cumpla la condicion. Fijate que la query es parametrizable con ?
<5> Si obtenemos resultados lo imprimimos por pantalla

Este script muestra cómo conectarnos y buscar un registro mediante una query parametrizable. Fijate que usamos el
caracter ? para indicar donde se deben de sustituir los parámetros. En este ejemplo usamos el día de ayer como
parámetro

