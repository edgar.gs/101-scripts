= Are you into the Panama Papers?
Jorge Aguilera, jorge.aguilera@puravida-software.com
2018-04-18
ifndef::backend-pdf[]
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc, gorm, neo4j, graph
:jbake-category: bbdd
:jbake-script: /scripts/bbdd/Neo4jGorm.groovy
:idprefix:
:imagesdir: ../images
:jbake-lang: gb
endif::[]

:icons: font
NOTE: GORM (Groovy o Grails?) Object Relational Mapping, http://gorm.grails.org/, is the library from Grails who allow us
to access and manage our database. At the beginning it only worked with Hibernate but recently it works with Neo4J,
NoSQL as MongoDB, etc

NOTE: Neo4j is a graph database management system developed by Neo4j, Inc. Described by its developers
as an ACID-compliant transactional database with native graph storage and processing,
 Neo4j is the most popular graph database according to DB-Engines ranking. (https://en.wikipedia.org/wiki/Neo4j)

NOTE: The Panama Papers are 11.5 million leaked documents that detail financial and attorney–client information for
more than 214,488 offshore entities. The documents, some dating back to the 1970s, were created by, and taken from,
Panamanian law firm and corporate service provider Mossack Fonseca, and were leaked in 2015 by an anonymous source.
(https://en.wikipedia.org/wiki/Panama_Papers)
{set:icons!:}


In this example we'll write a simple Groovy Script showing how to search into Panama Papers Neo4j database. Instead of
write a semi-complex Cipher Query we'll use GORM capabilities.

We will use the Docker image `ryguyrg/neo4j-panama-papers` instead of install a Neo4j database and populate it with the data,
because it's ready to use in only a few minutes (obviously you need Docker to run it):

[source,console]
----
docker run -p 7474:7474 -p 7687:7687 \      //<1>
        -d                          \       //<2>
        --name panama                \      //<3>
        -v $(pwd)/panama-papers-data:/data  //<4>
        ryguyrg/neo4j-panama-papers         //<5>
docker logs -f panama
----
<1> We need to link two ports, 7474 for browser and 7687 for bolt driver
<2> Launch in background
<3> name the container as panama (or whatever you want)
<4> to persist our changes in a local folder in case we remove the container
<5> the public Docker image


After launch the container we execute a docker command to inspect the logs of the container. In this way we can see
when the import process has finished. After them we need to change the password for the default user,
so you need to open a browser and navigate to
http://localhost:7474 . Login with the default user/password (neo4j/neo4j) and change the password.

In our case we'll use `panama` as password


== Dependencies

[source,groovy]
----
include::{sourcedir}{jbake-script}[tag=dependencias]
----

== Domain

The Panama Papers's domain is very simple. It has only 4 entities but a lot of relationships.

In the next diagram we'll show only a few of them.

[plantuml]
....
include::{sourcedir}{jbake-script}[tags=plantuml;!noplantuml]
Officer --> Entity : DIRECTOR_OF
Officer --> Entity : ASSISTANT_SECRETARY_OF
Officer --> Entity : BANK_SIGNATORY_OF
Intermediary --> Entity : SECRETARY_OF
Intermediary --> Entity : CUSTODIAN
....


== GORM

[source]
----
include::{sourcedir}{jbake-script}[tag=domain]
----
<1> As we'll use a domain object 'Entity' we rename the annotation from GORM as EntityAnnotation
<2> We'll declare relationships between entities as `hasMany`. In this example we'll declare only 1 relationship


== Command line arguments

Our script will search into the database looking for Officers or Entities by similar name passed as argument. Also we need to configure
the connection parameters as the url, user and password.

[source]
----
include::{sourcedir}{jbake-script}[tag=picocli]
----

== Configuration

[source]
----
include::{sourcedir}{jbake-script}[tag=configuration]
----

We need to configure the url, user and password and a list of the domain objects we want to GORM use.

== Simple Query

Once initialized the datasource, we can use GORM to perfom simple querys:

[source]
----
include::{sourcedir}{jbake-script}[tag=simpleQuery]
----
<1> perfom some Count
<2> as we are using a script we need to open a session with the database
<3> findByXXXX is added by GORM automatically
<4> we can navigate using relationships

== Complex Query

We can do more complicate querys using `where` for example to find similar Officer by similar names.

[source]
----
include::{sourcedir}{jbake-script}[tag=complexQuery]
----
<1> using `join` we instruct to GORM to fetch all objects in the same query

With this method we can instruct to Neo4J to look for officer without know the name:

[source]
----
groovy Neo4jGorm.groovy -p panama Macron

0: Macron Associate Company
         Craftlighting Inc., null

groovy Neo4jGorm.groovy -p panama Trump

0: Pacific Trump Enterprises Ltd
1: Trumptech (Far East) Limited
2: New Trump Technology Limited

groovy Neo4jGorm.groovy -p panama Kirchner

0: Guillermo Kirchner
         Meck Investments, Ltd, Corporate Solutions Inc. Corporate Solutions LLC 520 Brickell Key Dr   Ste 1403 Miami, Fl  33131 USA RT BVI
----


And you? are you in the Panama Papers ?





