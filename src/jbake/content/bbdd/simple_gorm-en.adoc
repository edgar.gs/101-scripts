= use GORM into your scripts
Jorge Aguilera, jorge.aguilera@puravida-software.com
2018-03-30
ifndef::backend-pdf[]
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc, gorm
:jbake-category: bbdd
:jbake-script: /scripts/bbdd/SimpleGorm.groovy
:idprefix:
:imagesdir: ../images
:jbake-lang: gb
:jbake-spanish: simple_gorm
endif::[]

NOTE: GORM (Groovy o Grails?) Object Relational Mapping, http://gorm.grails.org/, is the library from Grails who allow us
to access and manage our database. At the beginning it only worked with Hibernate but recently it works with Neo4J,
NoSQL as MongoDB, etc

In this example we'll write a simple Groovy Script showing how to do a Library with some functionalities from GORM


== Dependencies

[source,groovy]
----
include::{sourcedir}{jbake-script}[tag=dependencias]
----

== Domain

Our domain will be a _Library_ (Biblioteca in spanish) and _Books_ (Libro in spanish) where the first contains
a relationship with the second

[plantuml]
....
class Biblioteca{
    String nombre
}
class Libro{
    String codigo<P>
    String titulo
}
Biblioteca "1"-"*"Libro
....

We declare two @Entity classes with the desired attributes and relationships

[source,groovy]
----
include::{sourcedir}{jbake-script}[tag=domain]
----

GORM will find classes using @Entity annotation and wrap them with a lot of functions. In this scritp we'll not use
some of them as _validator_ and so on

== Configuration

We need to configure Hibernate in order to use a SQLite:

[source,groovy]
----
include::{sourcedir}{jbake-script}[tag=config]
----

We configure the url, the dialect and what classes we want to use with Hibernate (all @Entity classes)

== New Library

[source,groovy]
----
include::{sourcedir}{jbake-script}[tag=biblio]
----

== New Book

[source,groovy]
----
include::{sourcedir}{jbake-script}[tag=libros]
----

== Querying our Library

[source,groovy]
----
include::{sourcedir}{jbake-script}[tag=list]
----
<1> `select count(*) from biblioteca`
<2> `select * from biblioteca`
<3> `select * from libros where biblioteca_id = ?`
<4> findByXXX
<5> findAll
<6> _withCriteria_

== Test

[source,groovy]
----
include::{sourcedir}{jbake-script}[tag=main]
----




