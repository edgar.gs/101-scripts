= Biblioteca (docudocker)
Miguel Rueda <migue.rueda.garcia@gmail.com>
2018-03-05
ifndef::backend-pdf[]
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:jbake-category: docker
:jbake-script: /scripts/docker/DocuDocker.groovy
:idprefix:
:imagesdir: ../images
:jbake-lang: gb
:jbake-spanish: docudocker
endif::[]



A continuación se describe el caso de uso que vamos a desarrollar: Un cliente nos traslada la necesidad de crear
una biblioteca de documentos, donde colgar manuales o notas informativas para sus empleados. Esta información cambia
de manera mensual, tanto el contenido como la estructura de directorios. Además debe de correr en cualquier sistema
operativo ya que al trabajar con varios clientes cada uno cuenta con el suyo propio.

Nuestra solución se basará en una imagen Docker con un servidor web de contenido estático, donde habremos copiado los
documentos proporcionados por el cliente generando un image versionada con esta información.

Así pues nuestro sistema debe de realizar los siguientes pasos:

[plantuml]
....
actor User
participant Documentation
participant Repo
participant Docker
actor Client

User -> Documentation : scpDocs
Documentation -> Repo: changeVersion
Repo -> Repo : backe
Repo -> Docker : pushImage
Client -> Docker : pushImage
....


[sidebar]
Un cliente proporciona la nueva documentación para la biblioteca, esta es actualizada en el servidor preparado para ella
(con la función `scpDocuments`). Con la información ya en el servidor lanzamos la tareas de gradle (`createImage`) y
con ello obtenemos la imagen docker con nuestra biblioteca de documentos versionada en el repositorio
en el que estemos trabajando.

== Herramientas que vamos a emplear.

* *link:https://gitlab.com/jorge.aguilera/static-documents[DocuDocker]*:

En este enlace @jagedn explica paso a paso como crear una biblioteca con docker y jbake utilizando el proyecto
static-documents.

video::5x8i9Ft_29Y[youtube]

Dentro de este proyecto podemos encontrar el fichero *Dockerfile* que contiene la "receta" para generar
nuestra imagen docker de la biblioteca:

----
FROM httpd:2.4
EXPOSE 80
COPY . /usr/local/apache2/htdocs/
----

Este Dockerfile parte de la imagen de docker  _httpd:2.4_ y copia el contenido que tenemos en la ruta actual dentro
de su servidor apache. Por lo tanto a la hora de actualizar los documentos debemos centrarnos en este paso. En el ejemplo
sobre el que vamos a trabajar tiene la estructura: un directorio _RRHH_ (que contiene Convenio_Actual.pdf)
 y otro _Administración_ (que contiene  Cierre_mensual.pdf)

La herramientas que utiliza este proyecto es *gradle* y para la creación del entorno web estático es *JBake*.

== Preparación

Para la creación de nuestra biblioteca debemos ejecutar los siguientes pasos:

* Bajarnos en el servidor que deseamos crear la biblioteca el proyecto static-documents para posteriormente
realizar las tareas de gradle:

----
git clone https://gitlab.com/jorge.aguilera/static-documents.git
----

* Con el proyecto bajado debemos copiar dentro del proyecto en el directorio `src/documents/` la estructura
de directorios(en nuestro caso _RRHH_ y _Administración_ ) que deseamos encontrarnos en nuestra biblioteca ,
así como la información dentro de cada directorio.

* Cuando tengamos recopilada toda la información, ejecutar las tareas de gradle para generar la imagen docker.
Utilizaremos link:https://github.com/aestasit/sshoogr[sshoogr] ya que en este caso
que vamos a ver la información estará en un servidor remoto.

== Descripción del script

A continuación vamos a pasar a describir el script encargado de realizar cada una de las funciones descritas
anteriormente:

=== Dependencias y configuración necesarias.


Estas son las que vamos a necesitar:

[source,groovy]
----
@Grab('com.aestasit.infrastructure.sshoogr:sshoogr:0.9.25')
@GrabConfig(systemClassLoader=true)
import static com.aestasit.infrastructure.ssh.DefaultSsh.*

options.trustUnknownHosts = true //<1>

----

<1> Desactivación de la comprobación estricta de la clave del host


=== Actualización de versión.

El primer paso es eliminar del fichero `build.gradle` del proyecto los campos `version` y `tagVersion` para impedir
que en la creación de nuestra imagen tomen estas referencias a la hora de crear nuestra biblioteca. Nuestro script
creará un fichero _gradle.properties_ con la versión que hemos recibo por parametro y que será el tag de la imagen.

Utilizamos la función *updateVersion* que recibe por parámetro el servidor donde se encuentra el proyecto y la ruta
del fichero gradle.properties que será actualizado.

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=version]
----

=== Subida de nuevos documentos

Para este paso utilizaremos la función *scpDocuments*, a la cual le debemos indicar la ruta donde se encuentra
la documentación por parámetro y la ruta en el servidor destino

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=put_dir]
----


== Creación de la imagen docuDocker

Ya solo nos queda ejecutar las tareas gradle _build_ y _pushDockerRegistry_ (para subirla a nuestro repositorio).
Para ello realizaremos:

----
include::{sourcedir}{jbake-script}[tags=create]
----

En este punto una nueva imagen etiquetada se encuentra en nuestro repositorio y puede ser descargada por los
clientes para ver la última versión de los documentos

