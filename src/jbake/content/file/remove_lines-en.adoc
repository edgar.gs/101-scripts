= Delete lines with error
Jorge Aguilera <jorge.aguilera@puravida-software.com>
2018-02-01
ifndef::backend-pdf[]
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc,files, removeIf
:jbake-category: file
:jbake-script: /scripts/file/RemoveLines.groovy
:jbake-lang: gb
:idprefix:
:imagesdir: ../images
:jbake-spanish: remove-lines
endif::[]


Next we will see a really simple script for a frequently used case:

We have a file that contains a high number of lines and in another file we have identified the lines that we want delete
from the first one (maybe they are some IDs, names, etc).

The idea is that we would to go through the file that contains the records to be deleted and to search in the source
file those that comply with the condition by rewriting again and again the file until all the records of the error file
have been processed.

Through this script what we read the source file and convert it to a list (obviously this script is designed for files
with several thousand lines but with a size that does not exceed the available memory). After we will eliminate from
this list those records that meet the specific condition, for that we will use the method `removeIf` from Groovy

[source,groovy]
----
include::{sourcedir}{jbake-script}[]
----
<1> We assume that the first field of each line is the Id to search
<2> Remove from the list all the records that fulfill the condition of starting with the ID, for example.
<3> We generate a new file by joining the list with carriage returns
