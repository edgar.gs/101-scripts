= Scan and Execute
Jorge Aguilera <jorge.aguilera@puravida-software.com>
2017-09-16
ifndef::backend-pdf[]
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:jbake-category: file
:jbake-script: /scripts/file/ScanExecute.groovy
:jbake-lang: gb
:idprefix:
:imagesdir: ../images
:jbake-spanish: scan_execute
endif::[]


You will have to go through a files directory looking for those that meet a special condition and execute some command
if you find it. Sometimes the condition is simple, like searching for a text inside the file, if the filename
complies with a pattern, etc., but others will be somewhat more complicated.

Recently there was a need to upload all the contents of a maven repository in local (.m2) to a remote Nexus3 repository
so a simple copy & paste was not enough. Specifically, the conditions of the process were summarized in:

- the Nexus3 repository is protected by user / password
- search recursively in a given directory
- search if there is a POM file for the artifact
- check if there was a jar or a war with the same POM name
- if the pointed version is a snapshot they must be installed in a different repo that is a release
- execute the command _mvn deploy:deploy-file_ with the correct parameters

TIP: Obviously in *your case the requirements will be totally different* and you can probably solve it with a BASH but
such once the conditions get complicated it will be when you want to make a script like this

[source,groovy]
----
include::{sourcedir}{jbake-script}[]
----
<1> We check if there is a POM file
<2> We check if there is a Jar or War with the same name (indicates that they belong to the same version)
<3> We configure repository for snapshot or release
<4> We build a chain and execute it against the operating system
<5> We recursively go through the subdirectories
