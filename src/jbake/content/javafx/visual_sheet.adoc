= VisualSheet (Mejora la interface de tus scripts)
Jorge Aguilera <jorge.aguilera@puravida-software.com>
2018-02-17
ifndef::backend-pdf[]
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc, groogle, sheet, javafx
:jbake-category: javafx
:jbake-script: /scripts/javafx/VisualSheet.groovy
:idprefix:
:imagesdir: ../images
endif::[]



== Caso de Uso

Este script no tiene un caso de uso práctico como tal sino que pretende demostrar
la forma de mejorar el interface gráfico de los mismos para conseguir una mejor
interacción con el usuario.

En este caso vamos a mostrar un diálogo donde cargaremos en una tabla
una hoja de Google y el usuario podrá interactuar con los valores recuperados.

== Dependencias

Nuestro script va a necesitar las siguientes dependencias agrupadas por funcionalidad:

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=dependencies]
----

Por un lado vamos a necesitar las dependencias de GroovyFX para la parte visual y por otro vamos a necesitar
Groogle para la parte de interactuar con GoogleSheet

== Business

Nuestro negocio va a ser realmente muy simple. Vamos a representar cada fila de la hoja de cálculo mediante un
objeto *FxRow* el cual contiene dos valores recuperados de esta así como un valor calculado por aplicación (coordinate)
y otro que se actualiza tras la interacción del usuario con la fila en cuestión (derivate)

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=business]
----

Para la carga de este modelo vamos a usar una función _loadFile_ la cual leerá la hoja de cálculo e inicializará el
array de FxRows. Esta función será llamada únicamente cuando el usuario así lo desee pulsando un botón de la vista

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=load]
----

== Vista

La vista se compone de un botón que activará una *action* y una tabla que mostrará de forma dinámica el array de FxRows.
Así mismo podremos interactuar con los objetos en las columnas _name_ y _status_ de tal forma que podríamos actualizar
la hoja, calcular valores o en definitiva cualquier acción que requiera el negocio.

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=view]
----

Esto es una imagen de cómo podría quedar la aplicación:

image::visual_sheet.png[]

