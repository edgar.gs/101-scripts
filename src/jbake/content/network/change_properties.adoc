= Configuración dinámica de fichero remoto
Miguel Rueda <miguel.rueda.garcia@gmail.com>
2017-10-13
ifndef::backend-pdf[]
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:jbake-category: network
:jbake-script: /scripts/network/ChangeProperties.groovy
:idprefix:
:imagesdir: ../images
endif::[]


El caso que vamos a tratar es la modificación del contenido de un fichero con el formato clave:valor.

Si trabajamos
con varias máquinas, seguramente nos hayamos encontrado con que cada una de ellas tiene ficheros de configuración
que indiquen la url de la base de datos a la que debe conectectarse, messages.properties de nuestra aplicación ...

El problema de trabajar con estos ficheros es que en la mayoría de los casos, sobre todo cuando hablamos de ficheros
de configuración, son _personalizados_ y en el caso de tener que cambiar algún valor debemos de tener
mucho cuidado ya que no son ficheros que podamos copiar a todas nuestras maquinas de manera masiva porque dedemos
respetar la configuración que tienen cada uno de ellos.

A continuación vamos a explicar como crear un script que *modifique el usuario con el que hacemos la conexión a la
base de datos en base a cierta condición propia de la máquina*.

NOTE: Vamos a usar una herramienta que ya hemos utilizado
en alguno de nuestros scripts https://github.com/aestasit/sshoogr[sshoogr].

* Añadimos la dependencia de sshoogr
+
[source,groovy]
----
include::{sourcedir}{jbake-script}[tag=dependencies]
----

* Definimos el nombre del servidor y el de nuestro fichero de configuración.
+
[source,groovy]
----
include::{sourcedir}{jbake-script}[tag=config]
----

* Creamos el método de lectura de nuestro fichero
+
[source,groovy]
----
include::{sourcedir}{jbake-script}[tag=read]
----
<1> Establecemos los parámtros de conexión `usuario = user`, `contraseña = passwd` mientras que el `servidor` es variable.
<2> En la función `remoteFile` le pasamos por parámetro la ruta complete la fichero.
<3> Con la función `readLines` obtenemos una lista con el contenido del fichero.

* Creamos el método para sobreescribir el fichero con los datos actualizados
+
[source,groovy]
----
include::{sourcedir}{jbake-script}[tag=write]
----
<1> Establecemos los parámtros de conexión `usuario = user`, `contraseña = passwd` mientras que el `servidor` es variable.
<2> Actualizamos el contenido de nuestro fichero de configuración.

* Aplicamos la lógica de sustitución. En nuestro caso vamos a cambiar la variable `user.sql` por el valor de una
variable de entorno más una cadena fija `_changeme`
+
[source,groovy]
----
include::{sourcedir}{jbake-script}[tag=main]
----
<1> Lo primero que hacemos es emplear nuestra función `getFile` para obtener en contenido del fichero.
<2> Si econtramos la clave en nuestro fichero que queremos actualizar la modificamos y si no
saltamos a la siguiente linea.
<3> Llamaremos a nuestro método `putFile` el cúal nos actualizará el contenido de nuestro fichero de configuración.
+
NOTE: Si te da error al conectar con el host, prueba a desactivar la comprobación estricta de la clave del host:
+
[source,groovy]
----
options.trustUnknownHosts = true
----

