= Crear un fichero de texto con todas las operaciones disponibles en el API del INE
Jesús J. Ballano <jjballano@gmail.com>
2018-02-13
ifndef::backend-pdf[]
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc, files, rest
:jbake-category: network
:jbake-script: /scripts/network/HttpBuilderNG.groovy
:idprefix:
:imagesdir: ../images
:jbake-lang: es
:jbake-english: http_builder_ng_get-en
endif::[]



En este script vamos a guardar en un fichero de texto todas las operaciones disponibles que nos devuelve el INE (Instituto Nacional de Estadística de España) desde su endpoint http://servicios.ine.es//wstempus/js/ES/OPERACIONES_DISPONIBLES.

Crear ficheros de texto en Groovy es fácil y acceder a cualquier API REST con https://github.com/http-builder-ng/http-builder-ng[HttpBuilderNG] también, así que los vamos a combinar.

HttpBuilderNG es una librería que nos facilita mucho el acceso a cualquier API y que nos permite elegir entre 3 posibles implementaciones: `core`, `apache` o `okhttp`.
Para este ejemplo vamos a usar la implementación `core`.


[source,groovy]
----
include::{sourcedir}{jbake-script}[]
----

<1> Recogemos la librería de HttpBuilderNG.
<2> Creamos la configuración con la uri base.
<3> Ejecutamos la operación `get`.
<4> Accedemos al fichero donde queremos guardar las distintas operaciones.
<5> Por cada operación que nos devuelve el API, guardamos una línea en el fichero de texto.