= Desmenuzando un Pdf
Jorge Aguilera <jorge.aguilera@puravida-software.com>
2017-11-03
ifndef::backend-pdf[]
:jbake-type: post
:jbake-status: published
:jbake-tags: blog, asciidoc
:jbake-category: office
:jbake-script: /scripts/office/ExtractPdf.groovy
:idprefix:
:imagesdir: ../images
endif::[]




En ete script vamos a tratar las siguientes capacidades de Groovy:

- Incluir dependencias externas, en concreto PdfBox de Apache
- Realizar bucles anidados de una forma sencilla
- Generar mapas (key/value) al vuelo

== Objetivo

Mediante este script y usando la librería PdfBox de Apache, dividiremos las páginas de un Pdf en áreas de tal forma
que hagamos un "barrido" en cada una de ellas y podamos extraer el texto que se encuentre en las regiones. Al ir
haciendo el "barrido" podremos identificar cada línea leída con una posición dentro de la página


== Dependencias

Para la lectura del Pdf utilizaremos esta vez las librerías de Apache, PdfBox

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=dependencies]
----

== Script

[source,groovy]
----
include::{sourcedir}{jbake-script}[tags=sourceCode]
----
<1> Puedes "jugar" a definir el margen derecho para afinar cuanto texto tomar desde la derecha.
<2> Definimos 42 regiones de 20 pixeles, suficientes para un A4
<3> Definimos un área de 1500x20 suficientes para leer el ancho de un A4
<4> Para cada region que encuentra con texto la añadimos dinámicamente a un mapa creado para la página
<5> Para cada página añadimos las áreas encontradas en un mapa creado para todo el documento.


== En una linea (más Grappe)

@jmiguel en un afán minimalista nos comenta que el código anterior cumple su función
en una sóla línea:

[source,grooy]
----
println new PDFTextStripper().getText(PDDocument.load(new URL(args[0]).bytes))//<1>
----
<1>Si el primer argumento es una ruta local a un PDF, comienzalo con file://

