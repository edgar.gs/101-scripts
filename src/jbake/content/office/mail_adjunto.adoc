= Mail con documentos adjuntos
2017-08-04
ifndef::backend-pdf[]
:jbake-type: page
:jbake-status: draft
:jbake-tags: blog, asciidoc
:idprefix:
endif::[]


.Partimos de la clase Mail creada en el apartado anterior:
[source,console]
----
import javax.mail.*
import javax.mail.internet.*
import java.util.Properties;
import java.text.DecimalFormat;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


class Mail{
  def static sendEmail(path,subjetc,text,from,to){ //<1>

        def  d_email = from,
             d_uname = "",
             d_password = "",
             d_host = "smtp-relay.gmail.com",
             d_port  = "587", //465,587
             m_to = to,
             m_subject = subjetc,
             m_text = text

        def props = new Properties()
        props.put("mail.smtp.user", d_email)
        props.put("mail.smtp.host", d_host)
        props.put("mail.smtp.port", d_port)
        props.put("mail.smtp.starttls.enable","true")
        props.put("mail.smtp.debug", "false")
        props.put("mail.smtp.auth", "false")
        props.put("mail.protocol", "smtp")


        def session = Session.getInstance(props)
        session.setDebug(true);

        def msg = new MimeMessage(session)
        msg.setText(m_text)
        msg.setSubject(m_subject)
        msg.setFrom(new InternetAddress(d_email))
        msg.addRecipient(Message.RecipientType.TO, new InternetAddress(m_to))

         Multipart multipart = new MimeMultipart()
         def messageBodyPart = new MimeBodyPart()
         DataSource source = new FileDataSource(path) // <2>
         messageBodyPart.setDataHandler(new DataHandler(source))
         messageBodyPart.setFileName(filename)
         multipart.addBodyPart(messageBodyPart)
         msg.setContent(multipart)
        Transport.send( msg )
    }
}
----

<1> Incluir la parámetro: `path` donde se encuentra nuestro fichero
<2> En esta sea crea el objeto `DataSource`.



