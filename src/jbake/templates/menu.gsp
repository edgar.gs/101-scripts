<!-- Fixed navbar -->

<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<%if (content.rootpath) {%>${content.rootpath}<% } else { %><% }%>">101-scripts</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="<%if (content.rootpath) {%>${content.rootpath}<% } else { %><% }%>index.html" lang="es">Inicio</a></li>
                <li><a href="<%if (content.rootpath) {%>${content.rootpath}<% } else { %><% }%>scripts.html">Scripts</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" lang="es">Presentaciones <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <%slides.findAll{it.status=='published'}.each {ppt ->%>
                        <li><a lang="es" href="<%if (content.rootpath) {%>${content.rootpath}<% } else { %><% }%>${ppt.uri}">${ppt.title}</a></li>
                        <%}%>
                    </ul>
                </li>
                <li><a lang="es" href="<%if (content.rootpath) {%>${content.rootpath}<% } else { %><% }%>about.html">Acerca</a></li>
                <li><a lang="es" href="<%if (content.rootpath) {%>${content.rootpath}<% } else { %><% }%>${config.feed_file}">Subscribete</a></li>
            </ul>
            <form class="navbar-form navbar-right">
                <div class="form-group">
                    <button id="flagEn" type="button" class="btn btn-lan btn-default btn-group-lg flag-icon flag-icon-gb flag-icon-squared"onclick="return changeLang('gb');"></button>
                    <button id="flagEs" type="button"  class="btn btn-lan btn-default btn-group-lg flag-icon flag-icon-es flag-icon-squared" onclick="return changeLang('es');"></button>
                </div>
            </form>
            <form class="navbar-form navbar-right">
                <div class="form-group">
                    <span class="glyphicon glyphicon-search"></span>
                    <input id="search" type="search" name="q" lang="es" placeholder="Buscar..." maxlength="80" autocomplete="off" class="form-control">
                    <ul class="searchresults"></ul>
                </div>
            </form>
        </div><!--/.nav-collapse -->
    </div>
</div>
<div class="container-fluid">
    <div class="row">

        <h3>Fullmenu ${content.fullMenu}</h3>
    <%if(!content.fullMenu){%>
        <div class="hidden-xs hidden-sm col-md-2">
            <a class="twitter-timeline"
               data-tweet-limit="5"
               href="https://twitter.com/101GroovyScript?ref_src=twsrc%5Etfw">Tweets by @101GroovyScript</a>
            <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
        </div>
        <div class="col-md-1">&nbsp;</div>
        <div class="col-md-8">
    <%}else{%>
        <div class="col-md-1">&nbsp;</div>
        <div class="col-md-10">
    <%}%>